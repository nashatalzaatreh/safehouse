import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HousesController } from './houses/houses.controller';
import { HousesModule } from './houses/houses.module';

@Module({
  imports: [HousesModule],
  controllers: [AppController, HousesController],
  providers: [AppService],
})
export class AppModule {}
