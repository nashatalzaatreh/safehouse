import { Controller, Get } from '@nestjs/common';

@Controller('houses')
export class HousesController {
  @Get()
  getAllHouses() {
    return 'All Houses';
  }
}
