import { Module } from '@nestjs/common';
import { HousesController } from './houses.controller';

@Module({
  imports: [],
  controllers: [HousesController],
  //   providers: [HousesService],
})
export class HousesModule {}
